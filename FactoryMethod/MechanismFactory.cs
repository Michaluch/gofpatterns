using System;

namespace FactoryMethod
{
	public class MechanismFactory
	{
		public MechanismFactory ()
		{
		}

		public Mechanism getMechanism(String mechanismType)
		{
			if (mechanismType == null) {
				return null;
			}

			if (mechanismType.ToUpper () == "BALLISTA") {
				return new Ballista ();
			} else if (mechanismType.ToUpper () == "CATAPULT") {
				return new Catapult ();
			} else if (mechanismType.ToUpper () == "TREBUCHET") {
				return new Trebuchet ();
			}

			return null;
		}
	}
}

