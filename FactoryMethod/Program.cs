﻿using System;

namespace FactoryMethod
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			MechanismFactory mechanismFactory = new MechanismFactory ();

			Mechanism mechanism1 = mechanismFactory.getMechanism ("catapult");
			mechanism1.choice ();

			Mechanism mechanism2 = mechanismFactory.getMechanism ("ballista");
			mechanism2.choice ();

			Mechanism mechanism3 = mechanismFactory.getMechanism ("trebuchet");
			mechanism3.choice ();
		}
	}
}
	