using System;

namespace Facade
{
	public class HeroeFacade
	{
		private Heroe barbarian;
		private Heroe crusader;
		private Heroe wizard;

		public HeroeFacade ()
		{
			barbarian = new Barbarian ();
			crusader = new Crusader ();
			wizard = new Wizard ();
		}

		public void chooseBarbarian ()
		{
			barbarian.chooseHeroe ();
		}

		public void chooseCrusader ()
		{
			crusader.chooseHeroe ();
		}

		public void chooseWizard ()
		{
			wizard.chooseHeroe ();
		}
	}
}

