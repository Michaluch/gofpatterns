﻿using System;

namespace Facade
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			HeroeFacade facade = new HeroeFacade ();

			facade.chooseBarbarian ();
			facade.chooseCrusader ();
			facade.chooseWizard ();
		}
	}
}
