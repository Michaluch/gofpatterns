﻿using System;

namespace AbstractFactory
{
	public abstract class AbstractFactory
	{
		abstract public Race getRace(String race);
		abstract public Army getArmy(String army);
	}
}

