﻿using System;

namespace AbstractFactory
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			AbstractFactory raceFactory = FactoryProducer.getFactory ("RACE");

			Race race1 = raceFactory.getRace ("ORKS");
			race1.whoAmI ();

			Race race2 = raceFactory.getRace ("ELVES");
			race2.whoAmI ();

			Race race3 = raceFactory.getRace ("DWARFS");
			race3.whoAmI ();

			AbstractFactory armyFactory = FactoryProducer.getFactory ("ARMY");

			Army army1 = armyFactory.getArmy ("WARIOR");
			army1.setType();

			Army army2 = armyFactory.getArmy ("ARCHER");
			army2.setType();

			Army army3 = armyFactory.getArmy ("CAVALARY");
			army3.setType();
		}
	}
}
