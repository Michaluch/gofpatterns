﻿using System;

namespace AbstractFactory
{
	public class FactoryProducer
	{
		public FactoryProducer ()
		{
		}

		public static AbstractFactory getFactory(String choice)
		{
			if (choice.ToUpper() == "RACE") {
				return new RaceFactory ();
			} else if (choice.ToUpper() == "ARMY") {
				return new ArmyFactory ();
			}
			return null;
		}
	}
}

