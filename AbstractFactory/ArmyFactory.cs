﻿using System;

namespace AbstractFactory
{
	public class ArmyFactory : AbstractFactory
	{
		public ArmyFactory ()
		{
		}

		override public Race getRace (string raceType)
		{
			return null;
		}

		override public Army getArmy (string armyType)
		{
			if (armyType == null) {
				return null;
			}

			if (armyType.ToUpper () == "WARIOR") {
				return new Warior ();
			} else if (armyType.ToUpper () == "ARCHER") {
				return new Archer ();
			} else if (armyType.ToUpper () == "CAVALARY") {
				return new Cavalary ();
			}

			return null;
		}
	}
}

