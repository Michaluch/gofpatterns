﻿using System;

namespace AbstractFactory
{
	public class RaceFactory : AbstractFactory
	{
		public RaceFactory ()
		{
		}

		override public Race getRace(String raceType)
		{
			if (raceType == null) {
				return null;
			}

			if (raceType.ToUpper () == "ORKS") {
				return new Orks ();
			} else if (raceType.ToUpper () == "ELVES") {
				return new Elves ();
			} else if (raceType.ToUpper () == "DWARFS") {
				return new Dwarfs ();
			}

			return null;
		}

		override public Army getArmy (string armyType)
		{
			return null;
		}
	}
}

