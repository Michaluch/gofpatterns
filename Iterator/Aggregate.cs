using System;

namespace Iterator
{
	abstract class Aggregate
	{
		public abstract Iterator CreateIterator();
	}
}

