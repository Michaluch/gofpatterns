using System;
using System.Collections;

namespace Iterator
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			HeroesAggregate heroesStack = new HeroesAggregate();
			heroesStack[0] = "Jelu Devil's Bane";
			heroesStack[1] = "Catherine Ironfist";
			heroesStack[2] = "Gavin Magnus";
			heroesStack[3] = "Aeris AvLee";

			// Create Iterator and provide aggregate 
			HeroesIterator itemStack = new HeroesIterator(heroesStack);

			Console.WriteLine("Iterating over collection of heroes:");

			object item = itemStack.First();
			while (item != null)
			{
				Console.WriteLine(item);
				item = itemStack.Next();
			} 

			// Wait for user 
			Console.Read();
		}
	}
}