using System;
using System.Collections;

namespace Iterator
{
	class HeroesAggregate : Aggregate
	{
		private ArrayList items = new ArrayList();

		public override Iterator CreateIterator()
		{
			return new HeroesIterator(this);
		}

		// Property 
		public int Count
		{
			get{ return items.Count; }
		}

		// Indexer 
		public object this[int index]
		{
			get{ return items[index]; }
			set{ items.Insert(index, value); }
		}
	}
}

