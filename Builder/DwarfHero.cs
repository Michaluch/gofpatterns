using System;

namespace Builder
{
	public class DwarfHero : Heroes
	{
		public DwarfHero ()
		{
		}

		public override int dexterity()
		{
			return 4;
		}

		public override String name()
		{
			return "Darf Heroe";
		}
	}
}

