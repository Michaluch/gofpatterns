using System;

namespace Builder
{
	public abstract class Platoon : Stack
	{
		public abstract String name();

		public Movement movement()
		{
			return new Afoot ();
		}

		public abstract int dexterity();
	}
}

