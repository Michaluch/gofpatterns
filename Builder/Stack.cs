using System;

namespace Builder
{
	public interface Stack
	{
		String name ();
		Movement movement ();
		int dexterity ();
	}
}

