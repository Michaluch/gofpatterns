using System;

namespace Builder
{
	public class Archers : Platoon
	{
		public Archers ()
		{
		}

		public override int dexterity()
		{
			return 4;
		}

		public override String name()
		{
			return "Archers Platton";
		}
	}
}

