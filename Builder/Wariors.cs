using System;

namespace Builder
{
	public class Wariors : Platoon
	{
		public Wariors ()
		{
		}

		public override int dexterity()
		{
			return 2;
		}

		public override String name()
		{
			return "Wariors Platoon";
		}
	}
}

