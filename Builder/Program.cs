using System;

namespace Builder
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			ArmyBuilder armyBuild = new ArmyBuilder ();

			Army dwarfs = armyBuild.prepareDwarfsWariors ();
			Console.WriteLine ("Dwarfs Army:");
			dwarfs.showStackItems ();
			Console.WriteLine ("Total dexterity: " + dwarfs.getDexterity ());

			Army elves = armyBuild.prepareElvesArchers ();
			Console.WriteLine ("\n\nElves Army:");
			elves.showStackItems ();
			Console.WriteLine ("Total dexterity: " + elves.getDexterity ());
		}
	}
}
