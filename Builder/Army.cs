using System;
using System.Collections;
using System.Collections.Generic;

namespace Builder
{
	public class Army
	{
		public Army ()
		{
		}

		private List<Stack> stacks = new List<Stack> ();

		public void addStack(Stack stack)
		{
			stacks.Add (stack);
		}

		public int getDexterity()
		{
			int dexterity = 0;

			foreach (Stack stack in stacks) {
				dexterity += stack.dexterity ();
			}

			return dexterity;
		}

		public void showStackItems()
		{
			foreach (Stack stack in stacks) {
				Console.Write ("Stack name: " + stack.name ());
				Console.Write (", Movement: " + stack.movement());
				Console.WriteLine (", Dexterity: " + stack.dexterity());
			}
		}
	}
}

