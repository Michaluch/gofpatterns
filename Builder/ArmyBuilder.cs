using System;

namespace Builder
{
	public class ArmyBuilder
	{
		public Army prepareDwarfsWariors()
		{
			Army army = new Army ();
			army.addStack (new DwarfHero ());
			army.addStack (new Wariors ());
			return army;
		}

		public Army prepareElvesArchers()
		{
			Army army = new Army ();
			army.addStack (new ElveHeroe ());
			army.addStack (new Archers ());
			return army;
		}
	}
}

