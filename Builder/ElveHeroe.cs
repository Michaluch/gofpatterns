using System;

namespace Builder
{
	public class ElveHeroe : Heroes
	{
		public ElveHeroe ()
		{
		}

		public override int dexterity()
		{
			return 7;
		}

		public override String name()
		{
			return "Elves Heroe";
		}
	}
}

