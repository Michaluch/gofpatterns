using System;

namespace Mediator
{
	abstract class Colleague
	{
		protected Mediator mediator;

		// Constructor 
		public Colleague(Mediator mediator)
		{
			this.mediator = mediator;
		}
	}
}

