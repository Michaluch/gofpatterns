using System;

namespace Adapter
{
	public class FireSpells : MoreSpells
	{
		public FireSpells ()
		{
		}

		public void castFireSpell(String nameSpell)
		{
			Console.WriteLine ("Use fire spell: " + nameSpell);
		}

		public void castWaterSpell(String nameSpell)
		{

		}
	}
}

