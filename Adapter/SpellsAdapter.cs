using System;

namespace Adapter
{
	public class SpellsAdapter : NatureSpells
	{
		MoreSpells moreSpellsForHeroe;

		public SpellsAdapter (String typeSpell)
		{
			if (typeSpell.ToUpper () == "FIRE") {
				moreSpellsForHeroe = new FireSpells ();
			} else if (typeSpell.ToUpper () == "WATER") {
				moreSpellsForHeroe = new WaterSpells ();
			}
		}

		public void cast(String typeSpell, String nameSpell)
		{
			if (typeSpell.ToUpper () == "FIRE") {
				moreSpellsForHeroe.castFireSpell (nameSpell);
			} else if (typeSpell.ToUpper () == "WATER") {
				moreSpellsForHeroe.castWaterSpell (nameSpell);
			}
		}
	}
}

