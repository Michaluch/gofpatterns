using System;

namespace Adapter
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Mage greatMage = new Mage ();

			greatMage.cast ("Nature", "Stone Shield");
			greatMage.cast ("Fire", "FireBall");
			greatMage.cast ("Water", "Water Shield");
			greatMage.cast ("Chaos", "Armagedon");
		}
	}
}
