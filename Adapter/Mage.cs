using System;

namespace Adapter
{
	public class Mage : NatureSpells
	{
		SpellsAdapter splellAdapter;

		public void cast(String typeSpell, String nameSpell)
		{
			if (typeSpell.ToUpper () == "NATURE") {
				Console.WriteLine ("Use nature spell: " + nameSpell);
			} else if ((typeSpell.ToUpper () == "FIRE") || (typeSpell.ToUpper () == "WATER")) {
				splellAdapter = new SpellsAdapter (typeSpell);
				splellAdapter.cast (typeSpell, nameSpell);
			} else {
				Console.WriteLine ("I can't use this spell type: " + typeSpell);
			}
		}
		public Mage ()
		{
		}
	}
}

