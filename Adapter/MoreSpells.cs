using System;

namespace Adapter
{
	public interface MoreSpells
	{
		void castFireSpell(String nameSpell);
		void castWaterSpell(String nameSpell);
	}
}

