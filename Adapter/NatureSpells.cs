using System;

namespace Adapter
{
	public interface NatureSpells
	{
		void cast(String typeSpell, String nameSpell);
	}
}

