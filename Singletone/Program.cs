﻿using System;

namespace Singletone
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			SingletoneObject obj = SingletoneObject.getInstance ();

			obj.showMessage ();
		}
	}
}
