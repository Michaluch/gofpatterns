﻿using System;

namespace Singletone
{
	public class SingletoneObject
	{
		private static SingletoneObject instance = new SingletoneObject();

		private SingletoneObject(){}

		public static SingletoneObject getInstance()
		{
			return instance;
		}

		public void showMessage()
		{
			Console.WriteLine("Hello Singletone!");
		}
	}
}

