using System;

namespace Proxy
{
	public class RealImage : Image
	{
		private String fileName;

		public RealImage (String fileName)
		{
			this.fileName = fileName;
			uploadImage (fileName);
		}

		public void display()
		{
			Console.WriteLine ("Displaying " + fileName); 
		}

		private void uploadImage (String fileName)
		{
			Console.WriteLine ("Loading " + fileName);
		}
	}
}

