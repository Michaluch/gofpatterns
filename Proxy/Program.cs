﻿using System;

namespace Proxy
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Image image = new ProxyImage ("human_hero.jpg");

			Console.WriteLine ("Image will be loaded from disk: ");
			image.display ();

			Console.WriteLine ("\nImage will not be loaded from disk");
			image.display ();
		}
	}
}
