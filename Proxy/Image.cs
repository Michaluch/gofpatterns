using System;

namespace Proxy
{
	public interface Image
	{
		void display();
	}
}

