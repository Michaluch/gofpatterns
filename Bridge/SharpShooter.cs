using System;

namespace Bridge
{
	public class SharpShooter : PlatoonAPI
	{
		public SharpShooter ()
		{
		}

		public void abilityPlatoon(int dexterity, int accuracy, int wisdom)
		{
			Console.WriteLine ("Platoon type: 'Sharpshooter';");
			Console.WriteLine ("Ability:");
			Console.WriteLine ("Dexterity: " + dexterity);
			Console.WriteLine ("Wisdom: " + wisdom);
			Console.WriteLine ("Accuracy: " + accuracy);
		}
	}
}

