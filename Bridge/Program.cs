using System;

namespace Bridge
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Platoon eliteArcher = new Archer (3, 2, 1, new EliteArcher ());
			Platoon sharpShooter = new Archer (5, 7, 2, new SharpShooter ());

			eliteArcher.ability ();
			sharpShooter.ability ();
		}
	}
}
