using System;

namespace Bridge
{
	public class Archer : Platoon
	{
		private int dexterity, accuracy, wisdom;

		public Archer (int dexterity, int accuracy, int wisdom, PlatoonAPI platoonAPI)
		{
			this.platoonAPI = platoonAPI;
			this.accuracy = accuracy;
			this.dexterity = dexterity;
			this.wisdom = wisdom;
		}

		public override void ability()
		{
			platoonAPI.abilityPlatoon (dexterity, accuracy, wisdom);
		}
	}
}

