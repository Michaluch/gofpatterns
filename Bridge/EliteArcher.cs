using System;

namespace Bridge
{
	public class EliteArcher : PlatoonAPI
	{
		public EliteArcher ()
		{
		}

		public void abilityPlatoon(int dexterity, int accuracy, int wisdom)
		{
			Console.WriteLine ("Platoon type: 'Elite Archer';");
			Console.WriteLine ("Ability:");
			Console.WriteLine ("Dexterity: " + dexterity);
			Console.WriteLine ("Wisdom: " + wisdom);
			Console.WriteLine ("Accuracy: " + accuracy);
		}
	}
}

