﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Composite
{
	public class Heroes
	{
		private String name;
		private String race;
		private String stack;
		private List<Heroes> subordinates;

		public Heroes (String name, String race, String stack)
		{
			this.name = name;
			this.race = race;
			this.stack = stack;
			subordinates = new List<Heroes> ();
		}

		public void add (Heroes hero)
		{
			subordinates.Add (hero);
		}

		public void remove (Heroes hero)
		{
			subordinates.Remove (hero);
		}

		public List<Heroes> getSubordinates ()
		{
			return subordinates;
		}

		public override string ToString()
		{
			return ("Hero: [ Name : " + name + ", race : " + race + ", stack : " + stack + " ]");
		}
	}
}

