﻿using System;

namespace Composite
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Heroes armyCommander = new Heroes ("Jack Hornson", "Human", "Army commander");

			Heroes wariours = new Heroes ("Crag Hack", "Human", "Wariours commander");

			Heroes archers = new Heroes ("Jelu Devil's Bane", "Elve", "Archers commander");

			Heroes wariour1 = new Heroes ("Catherine Ironfist", "Human", "Wariour");
			Heroes wariour2 = new Heroes ("Gavin Magnus", "Human", "Wariour");

			Heroes archer1 = new Heroes ("Aeris AvLee", "Elve", "Archer");
			Heroes archer2 = new Heroes ("Erutan Revol", "Elve", "Archer");

			armyCommander.add (wariours);
			armyCommander.add (archers);

			wariours.add (wariour1);
			wariours.add (wariour2);

			archers.add (archer1);
			archers.add (archer2);

			Console.WriteLine (armyCommander);

			foreach (Heroes armyHead in armyCommander.getSubordinates())
			{
				Console.WriteLine (armyHead);

				foreach (Heroes army in armyHead.getSubordinates())
				{
					Console.WriteLine (army);
				}
			}
		}
	}
}
