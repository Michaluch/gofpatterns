using System;

namespace Decorator
{
	public abstract class RaceDecorator : Race
	{
		protected Race decoratedRace;

		public void setRace (Race decoratedRace)
		{
			this.decoratedRace = decoratedRace;
		}

		public void whoAmI()
		{
			decoratedRace.whoAmI ();
		}
	}
}

