﻿using System;

namespace Decorator
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Race human = new Human ();
			human.whoAmI ();
			Console.WriteLine ();

			WariourRaceDecorator humanWariour = new WariourRaceDecorator ();
			humanWariour.setRace (new Human ());

			WariourRaceDecorator elveWariour = new WariourRaceDecorator ();
			elveWariour.setRace (new Elves ());

			humanWariour.whoAmI ();
			elveWariour.whoAmI ();

		}
	}
}
